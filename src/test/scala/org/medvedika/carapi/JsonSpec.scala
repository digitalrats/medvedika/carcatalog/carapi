package org.medvedika.carapi

import java.sql.Timestamp
import java.util.Date

import io.circe.syntax._
import org.medvedika.carapi.domain.{Car, CarId}
import org.medvedika.carapi.interop.circe.JsonSupport
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.should.Matchers._

class JsonSpec extends AnyWordSpec with JsonSupport {
  "Json" when {
    "Timestam marshaller" should {
      val time = 1600661015739L
      "be able to marhsal Timestamp object" in {
        val ts = new Timestamp(time)
        val tsJson = ts.asJson
        tsJson.toString should equal ("1600661015739")
      }
      "be able to unmarhsal Timestamp object" in {
        val ts = new Timestamp(time)
        val tsJson = ts.asJson
        tsJson.as[Timestamp] match {
          case Left(error) =>  fail(error.message)
          case Right(value) => value should equal (new Timestamp(time))
        }
      }
    }
    "CarID marshaller" should {
      val id = "12345"
      "be able to marhsal CarID object" in {
        val carId = CarId(id)
        val idJson = carId.asJson
        idJson.toString should equal("{\n  \"value\" : \"12345\"\n}")
      }
      "be able to unmarhsal CarID object" in {
        val carId = CarId(id)
        val idJson = carId.asJson
        idJson.as[CarId] match {
          case Left(error) =>  fail(error.message)
          case Right(value) => value should equal (CarId(id))
        }
      }
    }

    "Car marshaller" should {
      val car = Car(CarId("12345"),"Model1","white",1979, new Timestamp(1600660351617L))
      "be able to marhsal Car object" in {
        val carJson = car.asJson
        val carStr = carJson.toString()
        carStr should equal("{\n  \"carId\" : {\n    \"value\" : \"12345\"\n  },\n  \"model\" : \"Model1\",\n  \"color\" : \"white\",\n  \"year\" : 1979,\n  \"created\" : 1600660351617\n}")
      }
      "be able to unmarhsal Car object" in {
        val carJson = car.asJson
        carJson.as[Car] match {
          case Left(error) =>  fail(error.message)
          case Right(value) => value should equal (car)
        }
      }
    }



  }

}
