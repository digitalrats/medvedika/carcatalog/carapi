package org.medvedika.carapi

import java.sql.Timestamp

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directive0
import akka.http.scaladsl.server.directives.{DebuggingDirectives, LoggingMagnet}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.medvedika.carapi.Server.LiveEnv
import org.medvedika.carapi.application.ApplicationSettings
import org.medvedika.carapi.domain.{Car, CarId}
import org.medvedika.carapi.infrastructure.dao.tables.CarsTable
import org.medvedika.carapi.interop.slick.dbio._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import slick.dbio.DBIO
import slick.jdbc.H2Profile.api._
import slick.lifted.TableQuery
import zio.{IO, Runtime, ZIO}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.Printer.noSpaces
import io.circe.{Json, parser}
import org.medvedika.carapi.interop.circe.JsonSupport

import scala.concurrent.ExecutionContextExecutor

class ApiSpec extends AnyWordSpec with JsonSupport with BeforeAndAfterAll with Matchers with ScalatestRouteTest {

  lazy val actorTestKit: ActorTestKit = ActorTestKit()
  implicit val actorSystem: ActorSystem[Nothing] = actorTestKit.system
  override def afterAll(): Unit = actorTestKit.shutdownTestKit()

  private val mokUser = "moyshe"
  private val mokPassword = "aeqrfdgh"
  private val mokFirstName = "Moyshe"
  private val mokLastName = "benRabbi"
  private val mokRole = "user"

  //private def now = new Timestamp(Calendar.getInstance().getTime.getTime)
  private def now = new Timestamp(0)


  /*
  private implicit val issuer: JWTIssuer = JWTIssuer("https://scaffold.me")
  private implicit val jwtkey: JWTKey = JWTKey(Vault.read("secret/carapi/auth","auth"))
  */

  override def beforeAll(): Unit = {
    val cars = TableQuery[CarsTable.Cars]
    val setup = {
      DBIO.seq(
        cars.schema.create,
        cars ++= Seq(
          Car(CarId("654321"), "ВАЗ 2105", "white", 1980, now),
          Car(CarId("465231"), "ВАЗ 2121", "red", 1987, now),
        )
      )
    }
    val setupIO: IO[Throwable, Unit] = ZIO.fromDBIO(setup).provide(liveEnv)
    Runtime.default.unsafeRun(setupIO)
  }

  implicit val ec: ExecutionContextExecutor = actorSystem.executionContext

  override def createActorSystem(): akka.actor.ActorSystem = actorTestKit.system.classicSystem

  implicit val settings: ApplicationSettings = ApplicationSettings(actorSystem)

  val liveEnv = new LiveEnv
  val api = new Api(liveEnv)

  def printRequestMethod(req: HttpRequest): Unit = println(req.method.name)
  val logRequestPrintln: Directive0 = DebuggingDirectives.logRequest(LoggingMagnet(_ => printRequestMethod))

  /// \todo Возможно, нужен актор, который инициализирует Ignite и создаст сервер. Теоретически в этом акторе ждать SIGINT

  "A Health" when {
    "health/status" should {
      "return DB status" in {
        Get("/health/status") ~> logRequestPrintln(api.route) ~> check {
          status should === (StatusCodes.OK)
          responseAs[Json].noSpaces should === ("{\"total\":2,\"firstRecordDate\":0,\"lastRecordDate\":0}")
        }
      }
    }
  }

  "A Cars" when {
    "cars" should {      "return car list StatusCode.OK" in {
        Get("/cars") ~> logRequestPrintln(api.route) ~> check {
          status should === (StatusCodes.OK)
          responseAs[Json].noSpaces should === ("[{\"carId\":{\"value\":\"654321\"},\"model\":\"ВАЗ 2105\",\"color\":\"white\",\"year\":1980,\"created\":0},{\"carId\":{\"value\":\"465231\"},\"model\":\"ВАЗ 2121\",\"color\":\"red\",\"year\":1987,\"created\":0}]")
        }
      }
      "return car list StatusCode.OK (cached)" in {
        Get("/cars") ~> logRequestPrintln(api.route) ~> check {
          status should === (StatusCodes.OK)
          responseAs[Json].noSpaces should === ("[{\"carId\":{\"value\":\"654321\"},\"model\":\"ВАЗ 2105\",\"color\":\"white\",\"year\":1980,\"created\":0},{\"carId\":{\"value\":\"465231\"},\"model\":\"ВАЗ 2121\",\"color\":\"red\",\"year\":1987,\"created\":0}]")
        }
      }
    }

    "cars" should {
      "be able to add cars (POST /cars) StatusCodes.OK" in {
        val car = Car(CarId("123456"), "УАЗ Hunter", "green", 2010, now)
        val carEntity = Marshal(car).to[MessageEntity].futureValue
        Post("/cars").withEntity(carEntity) ~> logRequestPrintln(api.route) ~> check {
          status should === (StatusCodes.Created)
        }
      }
    }

    "cars" should {
      "not be able to add cars with existing ids (POST /cars) StatusCodes.BadRequest" in {
        val car = Car(CarId("465231"), "УАЗ Hunter", "black", 2010, now)
        val carEntity = Marshal(car).to[MessageEntity].futureValue
        Post("/cars").withEntity(carEntity) ~> logRequestPrintln(api.route) ~> check {
          status should === (StatusCodes.BadRequest)
        }
      }
    }

    "return car list StatusCode.OK (cached cleared, item added)" in {
      Get("/cars") ~> logRequestPrintln(api.route) ~> check {
        status should === (StatusCodes.OK)
        responseAs[Json].as[List[Car]] match {
          case Left(error) => fail(error)
          case Right(value) => value.size should be (3)
        }
      }
    }

  }

  "A Car" when {
    "car" should {"be able to update cars with existing id (PUT /car/<carId>) StatusCodes.OK" in {
      val car = Car(CarId("465231"), "УАЗ Hunter", "black", 2010, now)
      val carEntity = Marshal(car).to[MessageEntity].futureValue
      Put("/car/123456").withEntity(carEntity) ~> logRequestPrintln(api.route) ~> check {
        status should ===(StatusCodes.OK)
      }
    }}

    "car" should {"be able to delete cars with existing id (DELETE/car/<carId>) StatusCodes.OK" in {
      Delete("/car/123456") ~> logRequestPrintln(api.route) ~> check {
        status should ===(StatusCodes.OK)
      }
    }}
  }

  "A Graphql" when {
    "graphql" should {"be able to query cars" in {
      val query = parser.parse("{\"query\":\"{allcars {carId {value} model color year created}}\"}")
      val queryEntity = Marshal(query).to[MessageEntity].futureValue
      Post("/graphql").withEntity(queryEntity) ~> logRequestPrintln(api.route) ~> check {
        status should ===(StatusCodes.OK)
        responseAs[Json].noSpaces shouldEqual "{\"data\":{\"allcars\":[{\"carId\":{\"value\":\"654321\"},\"model\":\"ВАЗ 2105\",\"color\":\"white\",\"year\":1980,\"created\":\"1970-01-01\"},{\"carId\":{\"value\":\"465231\"},\"model\":\"ВАЗ 2121\",\"color\":\"red\",\"year\":1987,\"created\":\"1970-01-01\"}]}}"
      }
    }}

    "graphql" should {"be able to add cars" in {
      val query = parser.parse("{\"query\":\"mutation addCar {createCar(carId: \\\"4324569\\\", model: \\\"LandRover Defender\\\", color: \\\"green\\\", year: 2014){carId { value }}}\"}")
      val queryEntity = Marshal(query).to[MessageEntity].futureValue
      Post("/graphql").withEntity(queryEntity) ~> logRequestPrintln(api.route) ~> check {
        status should ===(StatusCodes.OK)
        responseAs[Json].noSpaces shouldEqual "{\"data\":{\"createCar\":{\"carId\":{\"value\":\"4324569\"}}}}"
      }
    }}

    "graphql" should {"be able to remove cars" in {
      val query = parser.parse("{\"query\":\"mutation delCar {removeCar(carId: \\\"4324569\\\")}\"}")
      val queryEntity = Marshal(query).to[MessageEntity].futureValue
      Post("/graphql").withEntity(queryEntity) ~> logRequestPrintln(api.route) ~> check {
        status should ===(StatusCodes.OK)
        responseAs[Json].noSpaces shouldEqual "{\"data\":{\"removeCar\":1}}"
      }
    }}
  }

}
