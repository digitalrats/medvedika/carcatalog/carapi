package org.medvedika.carapi.interop.circe

import java.sql.Timestamp

import io.circe.Decoder.Result
import org.medvedika.carapi.{CreateCarRequest, UpdateCarRequest}
import org.medvedika.carapi.domain.{Car, CarId, CarStatus}

trait JsonSupport {
  import io.circe._, io.circe.generic.semiauto._
  implicit val TimestampFormat : Encoder[Timestamp] with Decoder[Timestamp] = new Encoder[Timestamp] with Decoder[Timestamp] {
    override def apply(a: Timestamp): Json = Encoder.encodeLong.apply(a.getTime)
    override def apply(c: HCursor): Result[Timestamp] = Decoder.decodeLong.map(s => new Timestamp(s)).apply(c)
  }

  implicit val carIdDecoder: Decoder[CarId] = deriveDecoder
  implicit val carIdEncoder: Encoder[CarId] = deriveEncoder
  implicit val carDecoder: Decoder[Car] = deriveDecoder
  implicit val carEncoder: Encoder[Car] = deriveEncoder
  implicit val carStatusDecoder: Decoder[CarStatus] = deriveDecoder
  implicit val caStatusEncoder: Encoder[CarStatus] = deriveEncoder

  implicit val createCarRequestDecoder: Decoder[CreateCarRequest] = deriveDecoder
  implicit val createCarRequestEncoder: Encoder[CreateCarRequest] = deriveEncoder
  implicit val updateCarRequestDecoder: Decoder[UpdateCarRequest] = deriveDecoder
  implicit val updateCarRequestEncoder: Encoder[UpdateCarRequest] = deriveEncoder

}
