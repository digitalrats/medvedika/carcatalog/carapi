package org.medvedika.carapi.interop.sql

import java.sql.Timestamp
import java.text.SimpleDateFormat

import sangria.marshalling.DateSupport
import sangria.schema.ScalarType
import sangria.validation.ValueCoercionViolation

import scala.util.{Failure, Success, Try}

trait TimestampSupport {

  val dateFormat = new SimpleDateFormat("yyyy-MM-dd")

  case object DateCoercionViolation extends ValueCoercionViolation("Date value expected")

  def parseDate(s: String): Either[DateCoercionViolation.type, Timestamp] = Try(new Timestamp(dateFormat.parse(s).getTime)) match {
    case Success(d) => Right(d)
    case Failure(error) => Left(DateCoercionViolation)
  }

  implicit val DateType: ScalarType[Timestamp] = ScalarType[Timestamp]("Date",
    coerceOutput = (d, caps) =>
      if (caps.contains(DateSupport)) d
      else dateFormat.format(d),
    coerceUserInput = {
      case s: String => parseDate(s)
      case _ => Left(DateCoercionViolation)
    },
    coerceInput = {
      case sangria.ast.StringValue(s, _, _ ,_ ,_) => parseDate(s)
      case _ => Left(DateCoercionViolation)
    })
}
