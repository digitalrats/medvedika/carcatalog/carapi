package org.medvedika

import io.circe.Json
import org.medvedika.carapi.domain.CarId

package object carapi {
  case class CreateCarRequest(carId: CarId, model: String, color: String, year: Short)
  case class UpdateCarRequest(model: String, color: String, year: Short)
  case class RemoveCarRequest()
  case class GraphQLRequest(query: String, operationName: Option[String], variables: Option[Json])
}
