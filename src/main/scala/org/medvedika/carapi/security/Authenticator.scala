package org.medvedika.carapi.security
/** Provides the authenticator implementation
 *
 *  ==Overview==
 *  The class to use is [[org.medvedika.carapi.security.Authenticator]].
 *  It is intended to use as an argument for authenticateOAuth2Async
 *
 */
import akka.http.scaladsl.server.Directives.AsyncAuthenticator
import akka.http.scaladsl.server.directives.Credentials

import scala.concurrent.Future

object Authenticator extends AsyncAuthenticator[Profile]  {
  def apply(v1: Credentials): Future[Option[Profile]] = ???
}
