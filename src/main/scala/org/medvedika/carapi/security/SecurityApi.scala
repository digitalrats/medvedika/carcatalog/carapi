package org.medvedika.carapi.security
/** Provides the security API routes
 *
 *  ==Overview==
 *  The class to use is [[org.medvedika.carapi.security.SecurityApi]].
 *
 */

import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, Routers}
import akka.actor.typed.{ActorSystem, SupervisorStrategy}
import akka.http.scaladsl.model.StatusCode
import akka.http.scaladsl.server.Directives.{as, complete, concat, entity, path, post}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.DebuggingDirectives
import akka.util.Timeout
import cats.data.EitherT
import cats.implicits._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import io.circe.syntax.EncoderOps
import org.medvedika.carapi.Server
import org.medvedika.carapi.application.ApplicationSettings
import org.medvedika.carapi.infrastructure.security.Vault
import org.medvedika.carapi.security.Token.{JWTIssuer, JWTKey}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

class SecurityApi(ctx: ActorContext[Server.Message])(implicit  ex: ExecutionContext) {

  private implicit val timeout: Timeout = 3 seconds
  private implicit val expirationOffset: Long = 157784760

  private implicit val issuer: JWTIssuer = JWTIssuer("https://digitalrats.ru")
  private implicit val key: JWTKey = JWTKey(Vault.read("","auth"))

  private implicit val system: ActorSystem[_] = ctx.system
  private val settings = ApplicationSettings(system)

  private lazy val authenticatorPool = Routers.pool(poolSize = 8)(
    // make sure the workers are restarted if they fail
    Behaviors.supervise(Authorizer()).onFailure[Exception](SupervisorStrategy.restart))

  private lazy val registrarPool = Routers.pool(poolSize = 8)(
    // make sure the workers are restarted if they fail
    Behaviors.supervise(Registrar()).onFailure[Exception](SupervisorStrategy.restart))

  private lazy val registrar = ctx.spawn(registrarPool, "Registrar")

  private lazy val authenticator = ctx.spawn(authenticatorPool, "worker-pool")

  DebuggingDirectives.logRequest("signup")
  DebuggingDirectives.logRequest("login")

  lazy val route: Route = concat(
    post {
      concat(
        path("signup") {
          entity(as[SignupRequest]) { request =>
            complete(
              registrar.ask[StatusCode](Registrar.SignRequest(request, _))
            )
          }
        },
        path("login") {
          entity(as[LoginRequest]) { request =>
            complete(
              (for {
                profile <- EitherT[Future, StatusCode, Profile](authenticator.ask(Authorizer.AuthRequest(request, _)))
                session <- EitherT(Session.create(request.login))
                token <- EitherT(Token.create(session))
                response <- EitherT(Future.successful({
                  LoginResponse(profile.role, token).asJson.asRight[StatusCode]
                }))
              } yield response).value
            )
          }
        }
      )
    }
  )
}
