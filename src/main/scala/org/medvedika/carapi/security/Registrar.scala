package org.medvedika.carapi.security
/** Provides the registrar actor implementation
 *
 *  ==Overview==
 *  The class to use is [[org.medvedika.carapi.security.Registrar]].
 *  It is intended to use as a part of security route implementation
 *
 */

object Registrar {

  import akka.actor.typed.scaladsl.Behaviors
  import akka.actor.typed.{ActorRef, Behavior, PostStop}
  import akka.http.scaladsl.model.{StatusCode, StatusCodes}
  import com.github.t3hnar.bcrypt._
  import org.slf4j.Logger

  final val defaultRole: String = "user"

  def apply(): Behavior[Request] = Behaviors.receive[Request] { (context, message) => {
    message match {
      case SignRequest(request, replyTo) =>
        replyTo ! signup(request.login, request.password, request.firstName, request.lastName, context.log)
        Behaviors.same
      case GracefulShutdown =>
        context.log.info("Initiating graceful shutdown...")
        Behaviors.stopped { () =>
          cleanup(context.log)
        }
    }
  }
  }.receiveSignal {
    case (context, PostStop) =>
      context.log.info("Master Control Program stopped")
      cleanup(context.log)
      Behaviors.same
  }

  private def signup(login: String, password: String, firstName: String, lastName: String, log: Logger): StatusCode = {
    try {
      val salt = generateSalt
      val hash = password.bcrypt(salt).replace(salt, "")
      val model = new Model(salt, hash, Profile(firstName, lastName, defaultRole))
      Model.auth.put(login, model)
      StatusCodes.OK
    } catch {
      case e: Throwable =>
        log.error(e.getLocalizedMessage)
        StatusCodes.BadRequest
    }
  }

  def cleanup(log: Logger): Unit = {
    log.info("Cleaning up!")
  }

  sealed trait Request

  final case class SignRequest(request: SignupRequest, replyTo: ActorRef[StatusCode]) extends Request

  final case object GracefulShutdown extends Request

}
