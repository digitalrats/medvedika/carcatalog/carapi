package org.medvedika.carapi.security
/** Provides the session implementation prototype
 *
 *  ==Overview==
 *  The class to use is [[org.medvedika.carapi.security.Session]].
 *  It is intended to use as a part of security route implementation
 *
 */

import java.util.UUID

final case class Session(id: UUID, expiration: Long)

object Session {

  import java.time.Instant

  import akka.http.scaladsl.model.StatusCode
  import cats.implicits._
  import io.circe.generic.auto._
  import io.circe.syntax._

  import scala.annotation.tailrec
  import scala.concurrent.Future

  /// \todo Брать это из конфигурации
  def create(login: String)(implicit expirationOffset: Long): Future[Either[StatusCode, Session]] = {
    val cache = Model.session

    /// \todo Скорее всего, этого мало - нужен сервис-синглетон с жесткой блокировкой на уровне кластера
    @tailrec
    def newUUID: UUID = {
      val id = UUID.randomUUID()
      if (!cache.containsKey(id)) id else newUUID
    }

    val expiration: Long = Instant.now.plusSeconds(expirationOffset).getEpochSecond
    /// \todo Нужен сервис для удаления просроченных сессий.
    val str = SessionBody(login, expiration).asJson.noSpaces
    val id = newUUID
    cache.put(id, str)
    Future.successful(Session(id, expiration).asRight[StatusCode])
  }

  private case class SessionBody(login: String, expiration: Long)
}

