package org.medvedika.carapi
/** Provides basic security implementation
 */

package object security {

  final case class LoginRequest(login: String, password: String)

  final case class LoginResponse(role: String, token: String)

  final case class SignupRequest(login: String, password: String, firstName: String, lastName: String)

  final case class Profile(firstName: String, lastName: String, role: String)

}
