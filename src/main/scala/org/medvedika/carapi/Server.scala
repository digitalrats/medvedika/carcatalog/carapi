package org.medvedika.carapi
/** Provides the entry point
 *
 *  ==Overview==
 *  The main class to use is [[org.medvedika.carapi.Server]]
 *
 */
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior, PostStop}
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.management.scaladsl.AkkaManagement
import org.medvedika.carapi.application.ApplicationSettings
import org.medvedika.carapi.infrastructure.dao.{LiveDatabaseProvider, SlickCarRepository}
import org.medvedika.carapi.infrastructure.dao.tables.CarsTable
import org.medvedika.carapi.interop.slick.dbio._
import org.medvedika.carapi.security.Model
import slick.lifted.TableQuery
import zio.{IO, Runtime, ZIO}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.io.StdIn
import scala.util.{Failure, Success, Try}

object Server {
  class LiveEnv
    extends SlickCarRepository
      with LiveDatabaseProvider

  def main(args: Array[String]): Unit = {
    // Activate Ignite client as soon as possible

    val HOST = sys.env.getOrElse("CARAPI_HOST","0.0.0.0")

    val system: ActorSystem[Server.Message] =
      ActorSystem(Server(HOST), "CarAPI")

    AkkaManagement(system).start()
    //ClusterBootstrap(system).start()

    def shutdown(): Unit = {
      system.terminate()
      Await.result(system.whenTerminated, 30 seconds)
    }

    scala.sys.addShutdownHook(() -> shutdown())

    println(s"Press RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    system.terminate()
  }

  def apply(host: String): Behavior[Message] = Behaviors.setup { ctx =>
    implicit val system: ActorSystem[Nothing] = ctx.system

    val liveEnv: LiveEnv = new LiveEnv
    val cars = TableQuery[CarsTable.Cars]

    val setup = {
      import slick.jdbc.H2Profile.api._
      DBIO.seq(
        cars.schema.create
      )
    }

    val setupIO: IO[Throwable, Unit] = ZIO.fromDBIO(setup).provide(liveEnv)
    Runtime.default.unsafeRun(setupIO)

    implicit val settings: ApplicationSettings = ApplicationSettings(ctx.system)
    implicit val ec: ExecutionContextExecutor = system.executionContext

    /*
    Try(new SecurityApi(ctx)) match {
      case Success(securityApi) =>
        val api = new Api(liveEnv)
        val serverBinding: Future[Http.ServerBinding] =
        Http.apply().newServerAt(host, settings.PORT).bindFlow(securityApi.route ~ api.route)
        ctx.pipeToSelf(serverBinding) {
          case scala.util.Success(binding) => Started(binding)
          case scala.util.Failure(ex) => StartFailed(ex)
        }
      case Failure(ex) =>
        ctx.pipeToSelf(Future.unit) {
          case scala.util.Success(_) => StartFailed(ex)
        }
    }
     */

    Try(new Api(liveEnv)) match {
      case Success(api) =>
        val serverBinding: Future[Http.ServerBinding] =
          Http.apply().newServerAt(host, settings.PORT).bindFlow(api.route)
        ctx.pipeToSelf(serverBinding) {
          case scala.util.Success(binding) => Started(binding)
          case scala.util.Failure(ex) => StartFailed(ex)
        }
      case Failure(ex) =>
        ctx.pipeToSelf(Future.unit) {
          case scala.util.Success(_) => StartFailed(ex)
        }
    }


    def running(binding: ServerBinding): Behavior[Message] =
      Behaviors.receiveMessagePartial[Message] {
        case Stop =>
          ctx.log.info(
            "Stopping server http://{}:{}/",
            binding.localAddress.getHostString,
            binding.localAddress.getPort)
          Behaviors.stopped
      }.receiveSignal {
        case (_, PostStop) =>
          Model.cleanup(system.log)
          binding.unbind()
          Behaviors.same
      }

    def starting(wasStopped: Boolean): Behaviors.Receive[Message] =
      Behaviors.receiveMessage[Message] {
        case StartFailed(cause) =>
          throw new RuntimeException("Server failed to start", cause)
        case Started(binding) =>
          ctx.log.info(
            "Server online at http://{}:{}/",
            binding.localAddress.getHostString,
            binding.localAddress.getPort)
          if (wasStopped) ctx.self ! Stop
          running(binding)
        case Stop =>
          starting(wasStopped = true)
      }

    starting(false)
  }

  sealed trait Message

  private final case class StartFailed(cause: Throwable) extends Message

  private final case class Started(binding: ServerBinding) extends Message

  case object Stop extends Message
}
