package org.medvedika.carapi.application

import akka.actor.ActorSystem

import scala.concurrent.Future

class ApplicationHealthCheck(system: ActorSystem) extends (() => Future[Boolean]) {
  override def apply(): Future[Boolean] = {
    /** \todo Релизовать это */
    Future.successful(true)
  }
}

