package org.medvedika.carapi.application

import org.medvedika.carapi.domain._
import zio.ZIO

object ApplicationService {
  def getCar(id: CarId): ZIO[CarRepository, DomainError, List[Car]] =
    ZIO.accessM[CarRepository](_.carRepository.getByCarId(id))

  def addCar(carId: CarId, model: String, color: String, year: Int): ZIO[CarRepository, DomainError, Car] =
    for {
      car <- ZIO.accessM[CarRepository](_.carRepository.add(carId, model, color, year))
    } yield car

  def updateCar(carId: CarId, model: String, color: String, year: Int): ZIO[CarRepository, DomainError, Int] =
    ZIO.accessM[CarRepository](_.carRepository.update(carId, model, color, year))

  def removeCar(carId: CarId): ZIO[CarRepository, DomainError, Int] =
    ZIO.accessM[CarRepository](_.carRepository.remove(carId))

  def getCars: ZIO[CarRepository, DomainError, List[Car]] =
    ZIO.accessM[CarRepository](_.carRepository.getAll)

  def getRowCount: ZIO[CarRepository, DomainError, Int] =
    ZIO.accessM[CarRepository](_.carRepository.getRowCount)

}