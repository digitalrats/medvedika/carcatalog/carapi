package org.medvedika.carapi.application

import java.util.concurrent.TimeUnit
import akka.actor.typed.{ActorSystem, Extension, ExtensionId}
import scala.concurrent.duration.Duration
import com.typesafe.config.{Config, ConfigFactory}

class ApplicationSettings(system: ActorSystem[_]) extends Extension {
  private final val config: Config = ConfigFactory.load(this.getClass.getClassLoader)
  final val VAULT_PATH = config.getString("api.auth.vault-path")
  final val REALM = config.getString("api.auth.realm")
  final val DB_HOST = config.getString("api.db.host")
  final val DB_PORT = config.getInt("api.db.port")
  final val DB_NAME = config.getString("api.db.name")
  final val CIRCUIT_BREAKER_TIMEOUT = Duration.create(config.getDuration(" api.circuit-breaker.timeout", TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS)
  final val PORT = config.getInt("api.port")
  final val ASK_TIMEOUT = config.getDuration("api.routes.ask-timeout")

  final val AUTHENTICATOR_POOL_SIZE = config.getInt( "api.auth.authenticator-pool-size")
  final val REGISTRAR_POOL_SIZE = config.getInt("api.auth.registrar-pool-size")

}

object ApplicationSettings extends ExtensionId[ApplicationSettings] {
  override def createExtension(system: ActorSystem[_]) = new ApplicationSettings(system)
}

