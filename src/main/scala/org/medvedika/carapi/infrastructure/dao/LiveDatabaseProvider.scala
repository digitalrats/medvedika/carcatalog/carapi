package org.medvedika.carapi.infrastructure.dao

import org.medvedika.carapi.interop.slick.DatabaseProvider
import slick.basic.BasicBackend
import slick.jdbc.JdbcBackend.Database
import zio.{UIO, ZIO}

trait LiveDatabaseProvider extends DatabaseProvider {
  override val databaseProvider: DatabaseProvider.Service = new DatabaseProvider.Service {
    override val db: UIO[BasicBackend#DatabaseDef] = ZIO.effectTotal(Database.forURL("jdbc:h2:mem:test1;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver"))
  }
}
