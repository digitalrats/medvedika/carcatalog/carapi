package org.medvedika.carapi.infrastructure.dao.tables

import java.sql.Timestamp
import java.util.Date

import org.medvedika.carapi.domain.{Car, CarId}
import org.medvedika.carapi.infrastructure.dao.EntityIdMappers._
import slick.jdbc.H2Profile.api._

object CarsTable {

  case class LiftedCar(carId: Rep[CarId], model: Rep[String], color: Rep[String], year: Rep[Int], created: Rep[Timestamp])

  implicit object CarShape extends CaseClassShape(LiftedCar.tupled, Car.tupled)


  class Cars(tag: Tag) extends Table[Car](tag, "cars") {
    def pk = primaryKey("pk_id", carId)

    def carId = column[CarId]("CAR_ID")

    def * = LiftedCar(carId, model, color, year, created)

    def model = column[String]("MODEL")

    def color = column[String]("COLOR")

    def year = column[Int]("YEAR")

    def created = column[Timestamp]("created")

  }

}
