package org.medvedika.carapi.infrastructure.dao


import java.sql.Timestamp
import java.time.OffsetDateTime
import java.util.{Calendar, Date}

import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException
import org.medvedika.carapi.domain.{Car, CarId, CarRepository, DomainError, IntegrityError, RepositoryError, ValidationError}

import org.medvedika.carapi.interop.slick.DatabaseProvider
import org.medvedika.carapi.interop.slick.dbio._
import slick.jdbc.H2Profile.api._
import zio.{IO, ZIO}

import EntityIdMappers._
import tables.CarsTable

trait SlickCarRepository extends CarRepository with DatabaseProvider {
  self =>
  private def now = new Timestamp(Calendar.getInstance().getTime.getTime)

  val cars = TableQuery[CarsTable.Cars]
  val carRepository: CarRepository.Service = new CarRepository.Service {

    override def add(carId: CarId, model: String, color: String, year: Int): IO[DomainError, Car] = {
      val query = cars
      val car = Car(carId, model, color, year, now)
      ZIO.fromDBIO(query += car).provide(self)
        .refineOrDie {
          case e: JdbcSQLIntegrityConstraintViolationException => IntegrityError(e)
          case e: Exception => RepositoryError(e)
        }.map(_ => car)
    }

    override def update(carId: CarId, model: String, color: String, year: Int): IO[DomainError, Int] = {
      val query = cars.filter(_.carId === carId)
        .map(u => (u.model, u.color, u.year))
      ZIO.fromDBIO(query.update((model, color, year))).provide(self)
        .refineOrDie {
          case e: Exception => RepositoryError(e)
        }
    }

    override def remove(carId: CarId): IO[DomainError, Int] = {
      val query = cars.filter(_.carId === carId)
      ZIO.fromDBIO(query.delete).provide(self)
        .refineOrDie {
          case e: Exception => RepositoryError(e)
        }
    }

    override def getByCarId(carId: CarId): IO[DomainError, List[Car]] = {
      val query = cars.filter(_.carId === carId)
      ZIO.fromDBIO(query.result).provide(self)
        .map(_.toList)
        .refineOrDie {
          case e: Exception => RepositoryError(e)
        }
    }

    override def getAll: IO[DomainError, List[Car]] = {
      val query = cars
      ZIO.fromDBIO(query.result).provide(self)
        .map(_.toList)
        .refineOrDie {
          case e: Exception => RepositoryError(e)
        }
    }

    def getRowCount: IO[DomainError, Int] = {
      val query = cars
      ZIO.fromDBIO(query.length.result).provide(self)
        .refineOrDie {
          case e: Exception => RepositoryError(e)
        }
    }

    def getFirstDate: IO[DomainError, Option[Timestamp]] = {
      val query = cars
      ZIO.fromDBIO(query.map(_.created).min.result).provide(self)
        .refineOrDie {
          case e: Exception => RepositoryError(e)
        }
    }

    def getLastDate: IO[DomainError, Option[Timestamp]] = {
      val query = cars
      ZIO.fromDBIO(query.map(_.created).max.result).provide(self)
        .refineOrDie {
          case e: Exception => RepositoryError(e)
        }
    }

  }

}
