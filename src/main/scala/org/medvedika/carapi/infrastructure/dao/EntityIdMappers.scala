package org.medvedika.carapi.infrastructure.dao

import org.medvedika.carapi.domain.CarId
import slick.jdbc.H2Profile.api._

object EntityIdMappers {
  implicit def carIdMapper: BaseColumnType[CarId] = MappedColumnType.base[CarId, String] (
    ent => ent.value,
    value => CarId(value)
  )
}

