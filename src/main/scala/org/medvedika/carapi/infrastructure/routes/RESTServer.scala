package org.medvedika.carapi.infrastructure.routes

import akka.http.scaladsl.model.headers.Location
import akka.http.scaladsl.model.{HttpResponse, StatusCodes, Uri}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directives, Route}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import org.medvedika.carapi.application.{ApplicationService, ApplicationSettings}
import org.medvedika.carapi.domain.{CarId, CarRepository}
import org.medvedika.carapi.infrastructure.LfuCache
import org.medvedika.carapi.interop.akka.ZioSupport
import org.medvedika.carapi.interop.circe.JsonSupport
import org.medvedika.carapi.{CreateCarRequest, UpdateCarRequest}

trait RESTServer extends LfuCache with Banner with ZioSupport with JsonSupport {
  protected override def route(env: CarRepository)(implicit ec: scala.concurrent.ExecutionContext,
                                 settings: ApplicationSettings): Route = {
    super.route(env) ~ pathPrefix("cars") {
      pathEnd {
        get {
          apiCache {
            complete(ApplicationService.getCars.provide(env))
          }
        } ~
          post {
            extractScheme { scheme =>
              extractHost { host =>
                entity(Directives.as[CreateCarRequest]) { req =>
                  ApplicationService.addCar(req.carId, req.model, req.color, req.year).provide(env).map { _ =>
                    respondWithHeader(Location(Uri(scheme = scheme).withAuthority(host, settings.PORT).withPath(Uri.Path(s"/car/${req.carId}")))) {
                      lfuCache.remove(Uri.Path(s"/cars"))
                      complete {
                        HttpResponse(StatusCodes.Created)
                      }
                    }
                  }
                }
              }
            }
          }
      }
    } ~
      pathPrefix("car") {
        path(Remaining) { carId =>
          get {
            apiCache {
              complete(ApplicationService.getCar(CarId(carId)).provide(env))
            }
          } ~
            put {
              entity(Directives.as[UpdateCarRequest]) { req =>
                lfuCache.remove(Uri.Path(s"/cars"))
                lfuCache.remove(Uri.Path(s"/car/$carId"))
                complete(ApplicationService.updateCar(CarId(carId), req.model, req.color, req.year).provide(env).map(_ => "{}"))
              }
            } ~
            delete {
              lfuCache.remove(Uri.Path(s"/cars"))
              lfuCache.remove(Uri.Path(s"/car/$carId"))
              complete(ApplicationService.removeCar(CarId(carId)).provide(env).map(_ => "{}"))
            }
        }
      }
  }
}
