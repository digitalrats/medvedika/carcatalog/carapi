package org.medvedika.carapi.infrastructure.routes

import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.syntax._
import io.circe.{Decoder, Encoder, Json}
import org.medvedika.carapi.GraphQLRequest
import org.medvedika.carapi.application.ApplicationSettings
import org.medvedika.carapi.domain.{CarRepository, CarSchema}
import org.medvedika.carapi.interop.akka.ZioSupport
import sangria.ast.Document
import sangria.execution._
import sangria.marshalling.circe._
import sangria.parser.QueryParser

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

trait GraphQLServer extends Banner with ZioSupport {
  implicit val GraphQLRequestDecoder: Decoder[GraphQLRequest] = deriveDecoder
  implicit val GraphQLRequestEncoder: Encoder[GraphQLRequest] = deriveEncoder

  protected override def route(env: CarRepository)(implicit ec: scala.concurrent.ExecutionContext,
                                 settings: ApplicationSettings): Route = super.route(env) ~ {
    (post & path("graphql")) {
      entity(akka.http.scaladsl.server.Directives.as[Json]) { requestJson =>
        endpoint(env, requestJson)
      }
    }
  }

  def endpoint(env: CarRepository, requestJSON: Json)(implicit ec: ExecutionContext): Route = {
    requestJSON.as[GraphQLRequest] match {
      case Left(error) => complete(BadRequest, error.getMessage.asJson)
      case Right(request) => QueryParser.parse(request.query) match {
        case Success(queryAst) =>
          complete(executeGraphQLQuery(env, queryAst, request.operationName, request.variables))
        case Failure(error) =>
          complete(BadRequest, error.getMessage.asJson)
      }
    }
  }

  private def executeGraphQLQuery(env: CarRepository, query: Document, operation: Option[String], vars: Option[Json])(implicit ec: ExecutionContext) = {
    vars match {
      case Some(v) => Executor.execute(
        CarSchema.SchemaDefinition,
        query,
        env,
        variables = v,
        operationName = operation
      ).map(OK -> _)
        .recover {
          case error: QueryAnalysisError => BadRequest -> error.resolveError
          case error: ErrorWithResolver => InternalServerError -> error.resolveError
        }
      case _ => Executor.execute(
        CarSchema.SchemaDefinition,
        query,
        env,
        operationName = operation
      ).map(OK -> _)
        .recover {
          case error: QueryAnalysisError => BadRequest -> error.resolveError
          case error: ErrorWithResolver => InternalServerError -> error.resolveError
        }
    }
  }

}
