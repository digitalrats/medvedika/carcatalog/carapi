package org.medvedika.carapi.infrastructure.routes

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives.{complete, pathEnd}
import akka.http.scaladsl.server.Route
import org.medvedika.carapi.application.ApplicationSettings
import org.medvedika.carapi.domain.CarRepository

trait Banner {
  protected def route(env: CarRepository)(implicit ec: scala.concurrent.ExecutionContext,
                        settings: ApplicationSettings): Route = pathEnd {
    /** \todo Тут может быть описание сервиса */
    complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Say hello to autoapi</h1>"))
  }
}
