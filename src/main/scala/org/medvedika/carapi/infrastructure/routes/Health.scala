package org.medvedika.carapi.infrastructure.routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import org.medvedika.carapi.application.ApplicationSettings
import org.medvedika.carapi.domain.{CarRepository, CarService}
import org.medvedika.carapi.interop.akka.ZioSupport
import org.medvedika.carapi.interop.circe.JsonSupport

trait Health extends Banner with ZioSupport with JsonSupport {
  protected override def route(env: CarRepository)(implicit ec: scala.concurrent.ExecutionContext,
                                 settings: ApplicationSettings): Route = super.route(env) ~ {
    pathPrefix("health") {
      path("status") {
        pathEnd {
          get {
            complete(CarService.calculateCarsStatus.provide(env))
          }
        }
      }
    }
  }
}
