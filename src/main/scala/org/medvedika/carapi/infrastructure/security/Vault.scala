package org.medvedika.carapi.infrastructure.security

/// \todo Тут можно добавить метод записи, тогда можно будет при первом запуске сервиса сгенерировать случайный ключ и реализовать их обновление через определенный период.
object Vault {

  import java.net.URL

  def read(path: String, key: String)(implicit vaultToken: VaultToken = VaultToken(sys.env("VAULT_TOKEN")),
                                      vaultUrl: VaultURL = VaultURL(new URL(sys.env.getOrElse("VAULT_HOST",
                                        "http://127.0.0.1:8200")))): String = {
    /// \todo Наивная реализация - ключ всегда читается из Vault при создание

    // Это использует API Vault (скорее всего) - возможно, стоит отказаться от использования сторонней библиотеки.
    // получить те же данные можно примерно так: curl -H "X-Vault-Token:s.gAsQYmsLyTp310rScJQHQtHn" -X GET http://localhost:8200/v1/secret/data/toolkit
    // Оно вернет что-то вроде:
    // {"request_id":"d722266f-fb36-6ede-fe05-39c16f902b90","lease_id":"","renewable":false,"lease_duration":0,"data":{"data":{
    // "auth":"ASdsew3231tfgd7659dsaferewfa23214afasd34SDfsdaW6Dfaasdwq3q4radfzxczvdf"},"metadata":{"created_time":"2020-04-23T
    // 03:59:42.6482566Z","deletion_time":"","destroyed":false,"version":1}},"wrap_info":null,"warnings":null,"auth":null}
    // - хороший повод использовать оптику (потому, что мне нужен оттуда только ключ "auth")

    import com.bettercloud.vault.VaultConfig

    val config = new VaultConfig().address(vaultUrl.value.getPath).
      token(vaultToken.value).build

    import com.bettercloud.vault.Vault
    val vault = new Vault(config)
    /// \todo Эта штука может вернуть null
    vault.logical.read(path).getData.get(key)
  }

  case class VaultToken(value: String)

  case class VaultURL(value: URL)

}
