package org.medvedika.carapi.infrastructure

import akka.actor.typed.ActorSystem
import akka.http.caching.LfuCache
import akka.http.caching.scaladsl.{Cache, CachingSettings}
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.server.{Directive0, RequestContext, RouteResult}
import akka.http.scaladsl.server.directives.CachingDirectives.cache

import scala.concurrent.duration.DurationInt

class LfuCache(implicit system: ActorSystem[Nothing] ) {
  // Use the request's URI as the cache's key
  private val keyerFunction: PartialFunction[RequestContext, Uri.Path] = {
    case r: RequestContext => r.request.uri.path
  }
  private val defaultCachingSettings = CachingSettings(system)
  private val lfuCacheSettings =
    defaultCachingSettings.lfuCacheSettings
      .withInitialCapacity(25)
      .withMaxCapacity(50)
      .withTimeToLive(20.seconds)
      .withTimeToIdle(10.seconds)
  private val cachingSettings =
    defaultCachingSettings.withLfuCacheSettings(lfuCacheSettings)

  val lfuCache: Cache[Uri.Path, RouteResult] = LfuCache(cachingSettings)
  // Create the route
  val apiCache: Directive0 = cache(lfuCache, keyerFunction)

}
