package org.medvedika.carapi
/** Provides the domain API implementation
 *
 *  ==Overview==
 *  The main class to use is [[org.medvedika.carapi.Api]], as so
 *  {{{
 *  scala> val api = new Api(liveEnv)
 *  scala> Http.apply().newServerAt(host, settings.PORT).bindFlow(api.route)
 *  }}}
 *
 */

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.DebuggingDirectives
import org.medvedika.carapi.application.ApplicationSettings
import org.medvedika.carapi.domain.CarRepository
import org.medvedika.carapi.infrastructure.LfuCache
import org.medvedika.carapi.infrastructure.routes.{Banner, GraphQLServer, Health, RESTServer}
import zio.Runtime
import zio.internal.Platform

import scala.concurrent.ExecutionContext

class Api(env: CarRepository)(implicit settings: ApplicationSettings, ec: ExecutionContext,
                              system: ActorSystem[Nothing] ) extends LfuCache
  with Banner
  with RESTServer
  with GraphQLServer
  with Health {

  override val environment: Unit = Runtime.default.environment
  override val platform: Platform = Runtime.default.platform

  DebuggingDirectives.logRequest("health")
  DebuggingDirectives.logRequest("cars")
  DebuggingDirectives.logRequest("car")

  lazy val route: Route = super.route(env)
}
