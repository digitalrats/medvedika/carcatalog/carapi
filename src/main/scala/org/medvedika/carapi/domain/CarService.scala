package org.medvedika.carapi.domain

import java.sql.Timestamp
import zio.ZIO

object CarService {
  def calculateCarsStatus: ZIO[CarRepository, DomainError, CarStatus] =
    for {
      count <- ZIO.accessM[CarRepository](_.carRepository.getRowCount)
      firstRecordDate <- ZIO.accessM[CarRepository](_.carRepository.getFirstDate)
      lastRecordDate <- ZIO.accessM[CarRepository](_.carRepository.getLastDate)
    } yield CarStatus(count, firstRecordDate.getOrElse(new Timestamp(0)), lastRecordDate.getOrElse(new Timestamp(0)))
}