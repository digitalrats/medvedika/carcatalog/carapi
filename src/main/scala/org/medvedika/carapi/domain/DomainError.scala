package org.medvedika.carapi.domain


sealed trait DomainError

case class RepositoryError(cause: Exception) extends DomainError

case class IntegrityError(cause: Exception) extends DomainError

case class ValidationError(msg: String) extends DomainError

