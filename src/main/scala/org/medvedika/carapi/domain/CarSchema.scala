package org.medvedika.carapi.domain

import org.medvedika.carapi.application.ApplicationService
import org.medvedika.carapi.interop.sql.TimestampSupport
import zio.Runtime
import zio.internal.Platform
// #
import sangria.macros.derive._
import sangria.schema._

object CarSchema extends Runtime[Unit] with TimestampSupport {
  override val environment: Unit = Runtime.default.environment
  override val platform: Platform = Runtime.default.platform

  implicit val CarIdType: ObjectType[Unit, CarId] = deriveObjectType[Unit, CarId]()
  val CarType: ObjectType[Unit, Car] = deriveObjectType[Unit, Car]()

  val QueryType: ObjectType[CarRepository, Unit] = ObjectType(
    "Query",
    fields[CarRepository, Unit](
      Field("allcars", ListType(CarType), resolve = c => {
        unsafeRun({
          ApplicationService.getCars.provide(c.ctx)
        })
      })
    )
  )

  val CarIdArg: Argument[String] = Argument("carId", StringType)
  val ModelArg: Argument[String] = Argument("model", StringType)
  val ColorArg: Argument[String] = Argument("color", StringType)
  val YearArg: Argument[Int] = Argument("year", IntType)

  val MutationType: ObjectType[CarRepository, Unit] = ObjectType(
    "Mutation",
    fields[CarRepository, Unit](
      Field("createCar",
        CarType,
        arguments = CarIdArg :: ModelArg :: ColorArg :: YearArg :: Nil,
        resolve = c => unsafeRun(ApplicationService.addCar(CarId(c.arg(CarIdArg)),c.arg(ModelArg), c.arg(ColorArg),c.arg(YearArg)).provide(c.ctx))
      ),
      Field("removeCar",
        IntType,
        arguments = CarIdArg :: Nil,
        resolve = c => unsafeRun(ApplicationService.removeCar(CarId(c.arg(CarIdArg))).provide(c.ctx))
      )
    )
  )

  val SchemaDefinition: Schema[CarRepository, Unit] = Schema(QueryType, Some(MutationType))
}
