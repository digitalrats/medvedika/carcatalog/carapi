package org.medvedika.carapi.domain

import java.sql.Timestamp

case class Car(carId: CarId, model: String, color: String, year: Int, created: Timestamp)

case class CarId(value: String) extends AnyVal

case class CarStatus(total: Int, firstRecordDate: Timestamp, lastRecordDate: Timestamp)