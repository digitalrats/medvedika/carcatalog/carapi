package org.medvedika.carapi.domain

import java.sql.Timestamp

import zio.{IO, ZIO}

trait CarRepository {
  def carRepository: CarRepository.Service
}

object CarRepository {

  trait Service {
    def add(carId: CarId, model: String, color: String, year: Int): IO[DomainError, Car]

    def update(carId: CarId, model: String, color: String, year: Int): IO[DomainError, Int]

    def remove(carId: CarId): IO[DomainError, Int]

    def getAll: IO[DomainError, List[Car]]

    def getByCarId(carId: CarId): IO[DomainError, List[Car]]

    def getRowCount: IO[DomainError, Int]

    def getFirstDate: IO[DomainError, Option[Timestamp]]

    def getLastDate: IO[DomainError, Option[Timestamp]]
  }

}