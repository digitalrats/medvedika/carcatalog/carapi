name := "carapi"

version := "0.1"

scalaVersion := "2.13.3"

val AkkaVersion = "2.6.8"
val AkkaHttpVersion = "10.2.0"
val AkkaManagementVersion = "1.0.8"
val ScalaTestVersion = "3.2.2"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  "-language:postfixOps",
  "-language:higherKinds")

libraryDependencies ++= Seq {
  "ch.qos.logback" % "logback-classic" % "1.2.3"
}

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-caching"% AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-testkit" % AkkaHttpVersion % "test"
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-slf4j",
  "com.typesafe.akka" %% "akka-actor-typed",
  "com.typesafe.akka" %% "akka-stream",
  "com.typesafe.akka" %% "akka-persistence",
  "com.typesafe.akka" %% "akka-cluster-typed",
  "com.typesafe.akka" %% "akka-cluster-sharding",
  "com.typesafe.akka" %% "akka-protobuf",
  "com.typesafe.akka" %% "akka-coordination",
  "com.typesafe.akka" %% "akka-discovery",
).map(_ % AkkaVersion)

libraryDependencies += "com.typesafe.akka" %% "akka-actor-testkit-typed" % AkkaVersion % "test"

libraryDependencies ++= Seq(
  "com.lightbend.akka.management" %% "akka-management",
  "com.lightbend.akka.management" %% "akka-management-cluster-http",
  "com.lightbend.akka.management" %% "akka-management-cluster-bootstrap",
  "com.lightbend.akka.discovery" %% "akka-discovery-consul"
).map(_ % AkkaManagementVersion)

libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic" % ScalaTestVersion,
  "org.scalatest" %% "scalatest" % ScalaTestVersion % "test"
)

val catsVersion = "2.2.0"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % catsVersion,
)

val zioVersion = "1.0.1"
libraryDependencies ++= Seq(
  "dev.zio" %% "zio",
  "dev.zio" %% "zio-streams"
).map(_ % "1.0.1")

libraryDependencies += "dev.zio" %% "zio-interop-reactivestreams" % "1.0.3.5"

val circeVersion = "0.13.0"
libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser",
).map(_ % circeVersion)

libraryDependencies ++= Seq(
  "com.pauldijou" %% "jwt-circe" % "4.3.0"
)

libraryDependencies ++= Seq(
  "de.heikoseeberger" %% "akka-http-circe" % "1.34.0"
)

libraryDependencies += "com.github.t3hnar" %% "scala-bcrypt" % "4.3.0"

libraryDependencies += "com.bettercloud" % "vault-java-driver" % "5.1.0"

val igniteVersion = "2.8.1"
libraryDependencies ++= Seq(
  "org.apache.ignite" % "ignite-core",
  "org.apache.ignite" % "ignite-slf4j"
).map(_ % igniteVersion)

val slickVersion = "3.3.3"
libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick",
  "com.typesafe.slick" %% "slick-hikaricp"
).map(_ % slickVersion)

libraryDependencies += "com.h2database" % "h2" % "1.4.200"

libraryDependencies += "org.sangria-graphql" %% "sangria" % "2.0.0"
libraryDependencies += "org.sangria-graphql" %% "sangria-marshalling-api" % "1.0.4"
libraryDependencies += "org.sangria-graphql" %% "sangria-circe" % "1.3.0"

Revolver.settings

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)